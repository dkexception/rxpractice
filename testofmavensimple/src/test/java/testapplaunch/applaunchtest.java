package testapplaunch;

import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class applaunchtest {
	@SuppressWarnings("rawtypes")
	public AppiumDriver driver;
	public DesiredCapabilities capabilities;
	@SuppressWarnings("rawtypes")
	@Test
	public void functiontest() {
		capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("platformVersion","8.0.0");
		capabilities.setCapability("deviceName","rem" );
		//capabilities.setCapability("udid","ce12171c3928401405" );
		capabilities.setCapability("fullReset", false);
		capabilities.setCapability("noReset", true);
		capabilities.setCapability("clearSystemFiles", true);
		capabilities.setCapability("newCommandTimeout", "1000000");
		capabilities.setCapability("appActivity", "com.dk.rxpractice.MainActivity");
		capabilities.setCapability("appPackage", "com.dk.rxpractice");
		capabilities.setCapability("automationName", "uiautomator2");
		try {
			driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}